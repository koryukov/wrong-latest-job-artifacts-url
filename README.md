# Wrong latest job artifacts URL

[Documentation says][doc]:
>>>
You can download job artifacts from the latest successful pipeline by using a URL.

To download the whole artifacts archive:

```
https://example.com/<namespace>/<project>/-/jobs/artifacts/<ref>/download?job=<job_name>
```
>>>

However, the reality is another.

## Steps to reproduce the bug

1. Fork the repository.
1. Create tag `test` on the head of the `main` branch.
1. Wait for CI/CD pipeline completed.
1. Make sure [jobs/artifacts/test/download?job=create+artifact](../-/jobs/artifacts/test/download?job=create+artifact) downloads archive *test.zip*.
1. Create tag `oops` on the head of the `main` branch.
1. Wait for CI/CD pipeline completed.
1. Make sure [jobs/artifacts/oops/download?job=create+artifact](../-/jobs/artifacts/oops/download?job=create+artifact) downloads archive *oops.zip*.
1. Follow the link [jobs/artifacts/test/download?job=create+artifact](../jobs/artifacts/test/download?job=create+artifact) again.

## Expected behavior

The *test.zip* archive should be downloaded.

## Actual behaviour

The *oops.zip* archive is downloaded.


[doc]: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#access-the-latest-job-artifacts-by-url
